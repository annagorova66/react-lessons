import {DEL_EMAIL_REQUEST, DEL_EMAIL_SUCCESS, READ_EMAIL, SEND_LETTER} from "../../actions/mails";

const initialState = {
    mailList: {
        getted: [
            {
                id: 1,
                status: false,
                from: 'test@test.ua',
                subject: 'Входящие',
                text: 'Если мы не реализовали функцию shouldComponentUpdate или же решили, что компонент должен обновиться в этом рендер цикле, вызовется другая функция жизненного цикла.'
            },

            {
                id: 2,
                status: false,
                from: 'test@test.ua',
                subject: 'Входящие Входящие',
                text: 'В случаях когда shouldComponentUpdate реализована, функция componentWillUpdate может быть использована вместо componentWillReceiveProps, т.к. она будет вызываться только тогда, когда компонент действительно будет перерисован.'
            },

            {
                id: 3,
                status: false,
                from: 'test@test.ua',
                subject: 'Входящие Входящие Входящие',
                text: 'Подобно всем другим componentWill* функциям, эта функция может быть вызывана несколько раз перед render, поэтому не рекомендуется выполнять здесь никакие операции вызывающие сайд-эффекты.'
            }
        ],
        sent: [
            {
                id: 4,
                status: true,
                from: 'test@test.ua',
                subject: 'Исходящие',
                text: 'Эта функция будет вызываться после того как отработала функция render, в каждом цикле перерисовки. Это означает, что вы можете быть уверены, что компонент и все его дочерние компоненты уже перерисовали себя.'
            },
            {
                id: 5,
                status: true,
                from: 'test@test.ua',
                subject: 'Исходящие Исходящие',
                text: 'В связи с этим эта функция является единственной функцией, что гарантировано будет вызвана только раз в каждом цикле перерисовки, поэтому любые сайд-эффекты рекомендуется выполнять именно здесь. '
            }
        ],
        draft: [
            {}
        ]
    },
    isFetching: false
}


function mails(state = initialState, action) {
    switch (action.type) {
        case READ_EMAIL:
            return {...state, ...action.payload}

        case DEL_EMAIL_REQUEST:
            return {...state, isFetching: true}

        case DEL_EMAIL_SUCCESS:
            return {...state, ...action.payload, isFetching: false}

        case SEND_LETTER:
            let newMailList = {
                getted: [...state.mailList.getted],
                sent: [...state.mailList.sent, action.payload]
            }

            return {...state, mailList: newMailList}

        default:
            return {...state}
    }
}

export default mails;