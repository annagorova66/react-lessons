import {combineReducers} from "redux";
import mails from './mails';
import {reducer as formReducer} from 'redux-form'

const rootReducer = combineReducers({
    mails,
    form: formReducer
})

export default rootReducer;