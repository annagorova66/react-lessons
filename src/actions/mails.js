export const DEL_EMAIL_SUCCESS = 'DEL_EMAIL_SUCCESS'
export const DEL_EMAIL_REQUEST = 'DEL_EMAIL_REQUEST'
export const READ_EMAIL = 'READ_EMAIL'
export const SEND_LETTER = 'SEND_LETTER'


export function delEmail(delID, mailList) {
    return dispatch => {
        dispatch({
            type: DEL_EMAIL_REQUEST,
        })
        setTimeout(() => {
            let newMailList = {...mailList }
            for(let key in newMailList){
                newMailList[key] = newMailList[key].filter( (item) => {
                    return item.id != delID ? item : null
                })
            }
            dispatch({ type: DEL_EMAIL_SUCCESS, payload: {mailList: newMailList} })
        }, 1000)
    }
}

export function sendLetter(mailInfo) {
    return dispatch =>{
        dispatch({
            type: SEND_LETTER,
            payload: mailInfo
        })
    }
}