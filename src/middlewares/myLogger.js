export const myLogger = store => next => action => {
    console.log(`Событие: ${action.type}, данные: `, action.payload)
    return next(action)
}