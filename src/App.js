import React, { Component, Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import Dashboard from './components/Dashboard'
import Contacts from './components/Contacts'

import './App.scss';

class App extends Component {
    render() {
        return(
            <Fragment>
                <Switch>
                    <Route exact path="/" component={Dashboard} />
                    <Route path="/mails" component={Dashboard} />
                    <Route path="/contacts" component={Contacts} />
                </Switch>
            </Fragment>
        )
    }
}

export default App;