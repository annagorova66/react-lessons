import React, {Component, Fragment} from 'react';
// import Menu from "../Menu";
// import Header from "../Header";
import {connect} from 'react-redux'

class Mail extends Component {
    // state = {
    //     active: 'getted',
    //     mailInfo: {
    //         from:'',
    //         text: ''
    //     },
    //     mailList: {
    //         getted: [
    //             {
    //                 id: 1,
    //                 status: false,
    //                 from: 'test@test.ua',
    //                 subject: 'Входящие',
    //                 text: 'Если мы не реализовали функцию shouldComponentUpdate или же решили, что компонент должен обновиться в этом рендер цикле, вызовется другая функция жизненного цикла.'
    //             },
    //
    //             {
    //                 id: 2,
    //                 status: false,
    //                 from: 'test@test.ua',
    //                 subject: 'Входящие Входящие',
    //                 text: 'В случаях когда shouldComponentUpdate реализована, функция componentWillUpdate может быть использована вместо componentWillReceiveProps, т.к. она будет вызываться только тогда, когда компонент действительно будет перерисован.'
    //             },
    //
    //             {
    //                 id: 3,
    //                 status: false,
    //                 from: 'test@test.ua',
    //                 subject: 'Входящие Входящие Входящие',
    //                 text: 'Подобно всем другим componentWill* функциям, эта функция может быть вызывана несколько раз перед render, поэтому не рекомендуется выполнять здесь никакие операции вызывающие сайд-эффекты.'
    //             }
    //         ],
    //         sent: [
    //             {
    //                 id: 4,
    //                 status: true,
    //                 from: 'test@test.ua',
    //                 subject: 'Исходящие',
    //                 text: 'Эта функция будет вызываться после того как отработала функция render, в каждом цикле перерисовки. Это означает, что вы можете быть уверены, что компонент и все его дочерние компоненты уже перерисовали себя.'
    //             },
    //             {
    //                 id: 5,
    //                 status: true,
    //                 from: 'test@test.ua',
    //                 subject: 'Исходящие Исходящие',
    //                 text: 'В связи с этим эта функция является единственной функцией, что гарантировано будет вызвана только раз в каждом цикле перерисовки, поэтому любые сайд-эффекты рекомендуется выполнять именно здесь. '
    //             }
    //         ]
    //     }
    // }

    // componentDidMount() {
    //     console.log('componentDidMount');
    //     let mailInfo = this.state.mailList.getted.filter((value)=>{
    //         return (value.id == Number(this.props.match.params.id));
    //     })
    //
    //     this.setState({
    //         mailInfo: mailInfo[0]
    //     })
    // }


    render() {
        let mailInfo = this.props.mails[this.props.match.params.folder].filter((value) =>{
            return (value.id === Number(this.props.match.params.id)) ? value : null
        })


        // let folderMail = " ";
        // if (mailInfo.length > 0){
        //     folderMail = (
        //         <Fragment>
        //             <h2>{mailInfo[0].subject}</h2>
        //             <p>{mailInfo[0].text}</p>
        //         </Fragment>
        //     )
        // } else{
        //     folderMail = "папка пуста";
        // }
        return (
            <Fragment>
                <div>
                    {/*{folderMail}*/}
                    <h2>{mailInfo[0].subject}</h2>
                    <p>{mailInfo[0].text}</p>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        mails: state.mails.mailList
    }
}

export default connect(mapStateToProps)(Mail)
// export default Mail;