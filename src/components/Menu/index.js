import React, { Component, Fragment } from 'react';
import {NavLink} from "react-router-dom";
import './menu.scss';

class Menu extends Component{
    state ={
        folders: [
            {
                name: 'Входящие',
                id: 'getted'
            },
            {
                name: 'Исходящие',
                id: 'sent'
            },
            {
                name: 'Черновик',
                id: 'draft'
            }
        ]
    }

    render() {
        let folderList = this.state.folders.map( (item) => {
            let folderURL = '/mails/' + item.id
            let classN = 'menu-folder';
            // if(item.id == this.props.active){
            //     classN = "active"
            // }
            return (<li key={item.id} // onClick={() => this.props.chgFolder(item.id)}
                    >
                        <NavLink
                            className={classN}
                            to={folderURL}>{item.name}
                        </NavLink>
                    </li>)
                    })
        return(
            <Fragment>
                <div className='block-menu'>
                    <ul>
                        {folderList}
                    </ul>
                </div>
            </Fragment>
        )
    }
}

export default Menu;