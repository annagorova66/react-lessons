import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import myInput from '../Atomic/myInput'
import customTextarea from '../Atomic/customTextarea'
import './NewLetter.scss'


class NewLetterForm extends Component {
    render(){
        console.log(this.props);
        const {handleSubmit} = this.props; // ф-ція handleSubmit вбудована,  що робити з даними, куди їх відправляти, як оброблювати
        let hiddenForm = !this.props.active ? 'form-new-letter' : 'form-hidden' ;

        return (
            <form onSubmit={handleSubmit} className={hiddenForm}>
                <Field name="emailTo" component={myInput}
                       type="text" placeholder="Your e-mail..."
                />
                <Field name="subject" component={myInput}
                       type="text" placeholder="Your e-mail..."
                />
                <Field name="textMsg" component={customTextarea}
                       type="textarea" placeholder="Message" />
                <button type="submit" label="submit">Send</button>
            </form>
        );
    }
}
export default reduxForm({ form: 'new-letter' })(NewLetterForm);