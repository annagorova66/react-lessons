import React, { Component } from 'react'
import {connect} from 'react-redux'
import NewLetterForm from './NewLetterForm'
import {sendLetter} from "../../actions/mails";

class NewLetter extends Component {
    state = {
        emailTo: '',
        subject: '',
        textMsg: ''
    }

    handleSubmit = values => {
        console.log(values)
        this.props.sendLetter(values)
    };


    render() {
        return (
            <div>
                {/*<h1>Новое сообщение</h1>*/}
                <NewLetterForm onSubmit={this.handleSubmit}/>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        sendLetter: (mailInfo) => dispatch(sendLetter(mailInfo))
    }
}

export default connect(null, mapDispatchToProps)(NewLetter);


// import React, {Component, Fragment} from 'react';
// import './NewLetter.scss';
//
// class NewLetter extends Component{
//     state = {
//         emailTo: '',
//         subject: '',
//         textMsg: ''
//     }
//
//     changeDateLetter = event =>{
//         const name = event.target.name;
//         const value = event.target.value;
//         this.setState({
//             [name]: value
//         })
//
//     }
//
//     goNewLetter = event =>{
//         event.preventDefault();
//         this.props.sendNewLetter(this.state);
//
//         this.props.toggleNewLetter(false);
//     }
//
//     closeNewLetter = () =>{
//         let subjectCloseLetter = {
//             emailTo: '',
//             subject: '',
//             textMsg: ''
//         }
//
//         this.setState({
//             state:{
//                 subjectCloseLetter
//             }
//         })
//
//         this.props.toggleNewLetter(false);
//     }
//
//
//     render() {
//         let hiddenForm = this.props.active ? 'form-new-letter' : 'form-hidden' ;
//         return(
//             <Fragment>
//                 <form className={hiddenForm} action='#'>
//                     <input
//                         className='letter-for'
//                         type='text'
//                         name='emailTo'
//                         value={this.state.emailTo}
//                         onChange={this.changeDateLetter}
//                         placeholder='Кому'/>
//                     <input
//                         className='letter-for'
//                         type='text'
//                         name='subject'
//                         value={this.state.subject}
//                         onChange={this.changeDateLetter}
//                         placeholder='тема'/>
//                     <textarea
//                         className='letter-desc'
//                         name='textMsg'
//                         onChange={this.changeDateLetter}
//                     >{this.state.textMsg}</textarea>
//
//                     <div className='form-btn'>
//                         <button type='submit' onClick={this.goNewLetter}>Отправить</button>
//                         <button type='button' onClick={this.closeNewLetter}>Отмена</button>
//                     </div>
//
//
//                 </form>
//
//
//
//             </Fragment>
//         )
//     }
// }
// export default NewLetter;