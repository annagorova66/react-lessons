import React, {Component} from 'react';
import './BtnNewLetter.scss';

class BtnNewLetter extends Component{
    render() {
        return(
            <button type='button' className='btn-new-letter' onClick={()=>this.props.toggleNewLetter(this.props.children)}>Написать</button>
        )
    }
}

export default BtnNewLetter;