import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import Search from "./Search";
import "./header.scss";
import UserControl from "./UserControl";

class Header extends Component{

    render() {
        return(
            <header>
                <div className="logo">

                </div>
                <div className="search-block">
                    <Search/>
                </div>

                <div className="control">
                    {/*<NavLink to="/contacts">Dashboard </NavLink>*/}
                    {/*<NavLink to="/">click </NavLink>*/}
                    {/*<UserControl/>*/}
                </div>
            </header>
        )
    }
}

export default Header;