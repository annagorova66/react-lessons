import React, {Component, Fragment} from 'react';
import { Route } from 'react-router-dom';

import Header from '../Header';
import Menu from "../Menu";
import Mail from '../Mail'
import MailList from "../MailList";
import NewLetter from "../NewLetter";
import BtnNewLetter from "../NewLetter/BtnNewLetter";

class Dashboard extends Component {
    state = {
        openFormLetter: false,
        active: 'getted',
        mailList: {
            getted: [
                {
                    id: 1,
                    status: false,
                    from: 'test@test.ua',
                    subject: 'Входящие',
                    text: 'Если мы не реализовали функцию shouldComponentUpdate или же решили, что компонент должен обновиться в этом рендер цикле, вызовется другая функция жизненного цикла.'
                },

                {
                    id: 2,
                    status: false,
                    from: 'test@test.ua',
                    subject: 'Входящие Входящие',
                    text: 'В случаях когда shouldComponentUpdate реализована, функция componentWillUpdate может быть использована вместо componentWillReceiveProps, т.к. она будет вызываться только тогда, когда компонент действительно будет перерисован.'
                },

                {
                    id: 3,
                    status: false,
                    from: 'test@test.ua',
                    subject: 'Входящие Входящие Входящие',
                    text: 'Подобно всем другим componentWill* функциям, эта функция может быть вызывана несколько раз перед render, поэтому не рекомендуется выполнять здесь никакие операции вызывающие сайд-эффекты.'
                }
            ],
            sent: [
                {
                    id: 4,
                    status: true,
                    from: 'test@test.ua',
                    subject: 'Исходящие',
                    text: 'Эта функция будет вызываться после того как отработала функция render, в каждом цикле перерисовки. Это означает, что вы можете быть уверены, что компонент и все его дочерние компоненты уже перерисовали себя.'
                },
                {
                    id: 5,
                    status: true,
                    from: 'test@test.ua',
                    subject: 'Исходящие Исходящие',
                    text: 'В связи с этим эта функция является единственной функцией, что гарантировано будет вызвана только раз в каждом цикле перерисовки, поэтому любые сайд-эффекты рекомендуется выполнять именно здесь. '
                }
            ],
            draft:[
                {

                }
            ]

        }
    }

    componentDidMount() {
        console.log('componentDidMount');

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('componentDidUpdate');

        let lengthGetted = this.state.mailList.getted.length
        let lengthSent = this.state.mailList.sent.length
        let lengthDraft = this.state.mailList.draft.length

        console.log('getted = ', lengthGetted);
        console.log('sent = ', lengthSent);
        console.log('draft = ', lengthDraft);
    }

    chgFolder = (folderId) => {
        this.setState({
            active: folderId
        })
    }

    // sendNewLetter = (msg) =>{
    //     let mailListUpd = this.state.mailList.sent;
    //     let newLetter = {
    //         id: Math.random() * (100 - 10) + 10,
    //         status: true,
    //         from: msg.emailTo,
    //         subject: msg.subject,
    //         text: msg.textMsg
    //     }
    //     mailListUpd.push(newLetter);
    //
    //     this.setState({
    //         mailList:{
    //             ...this.state.mailList,
    //             [this.state.mailList.sent]: mailListUpd
    //         }
    //     })
    // }

    readMail = (id) => {
        let mailListUpd = this.state.mailList[this.state.active].map((item) => {
            let newObj = {...item};
            if (newObj.id === id){
                newObj.status = true
            }
            return newObj
        });

        this.setState({
            mailList: {
                ...this.state.mailList,
                [this.state.active]: mailListUpd
            }
        })
    }

    toggleNewLetter = () =>{
        this.setState({
            openFormLetter: !this.state.openFormLetter
        })
    }

    render() {
        let activeCategory = this.state.active;
        return (
            <Fragment>
                <Header/>
                <div className="mail-region">
                    <div className='sidebar'>
                        <BtnNewLetter toggleNewLetter={this.toggleNewLetter}/>
                        <Menu chgFolder={this.chgFolder} active={activeCategory}/>
                    </div>

                    <div className='block-maillist'>

                        <Route exact path="/mails/:folder/:id" render={(props) => {
                            return <Mail {...props} />
                        }} />

                        <Route exact path="/mails/:folder" render={(props) => {
                            return <MailList {...props} />
                        }} />

                        <Route exact path="/" render={(props) => {
                            return <MailList {...props} />
                        }} />

                    </div>

                    {/*<MailList*/}
                        {/*mails={this.state.mailList[activeCategory]}*/}
                        {/*readMail={this.readMail}/>*/}
                    <NewLetter active={this.state.openFormLetter} toggleNewLetter={this.toggleNewLetter}  />

                </div>
            </Fragment>
        )

    }
}

export default Dashboard;