import React, { Fragment } from 'react';

const customTextarea = (props) =>{
    const {input, type, placeholder, meta} = props;
    const classN = 'letter-desc'
    return (
        <Fragment>
            <textarea {...input} type={type} placeholder={placeholder}  className={classN}/>
            {meta.error && meta.touched && <div>{meta.error}</div>}
        </Fragment>
    )
}
export default customTextarea;
