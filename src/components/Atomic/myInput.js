import React, { Fragment } from 'react';

const myInput = (props) =>{
    const {input, type, placeholder, meta} = props;
    const classN = 'letter-for';
    return (
        <Fragment>
            <input {...input} type={type} placeholder={placeholder} className={classN}/>
            {meta.error && meta.touched && <div>{meta.error}</div>}
        </Fragment>
    )
}
export default myInput;