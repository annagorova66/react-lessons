import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import './MailList.scss';
import {connect} from 'react-redux';
import {delEmail} from "../../actions/mails";


class MailList extends Component {
    // state = {
    //     openedMsg: []
    // }
    // toggleMsg = (item) =>{
    //     let temple = this.state.openedMsg;
    //
    //     if (temple.includes(item.id)){
    //         temple.splice(temple.indexOf(item.id), 1);
    //     } else{
    //         if(!item.status){
    //             this.props.readMail(item.id)
    //         }
    //         temple.push(item.id);
    //     }
    //
    //     this.setState({
    //         openedMsg: temple
    //     })
    // }
    // getShowMsg = (item) => {
    //     let opnMsg = this.state.openedMsg;
    //     let classShow = opnMsg.includes(item) ? null : 'hidden';
    //
    //     return classShow;
    // }

    render() {
        let folder = this.props.match.params.folder ? this.props.match.params.folder : 'getted'


        let mailList = this.props.mails[folder].map((item) => {
        let mailsURL = `/mails/${folder}/${item.id}`
            return (
                <div className='block-item-letter'>
                    <input type='checkbox' className='box-choose'/>

                    <li key={item.id}
                        // onClick={() => this.toggleMsg(item)}
                        className={item.status ? 'item-letter' : 'item-letter active-letter'}
                    >
                        <NavLink to={mailsURL}>
                <span className='letter-from'>{item.from}</span>
                <span className='letter-title'>{item.subject}</span>
                        </NavLink>
                        {/*<p className={this.getShowMsg(item.id)}>{item.text}</p>*/}
                        <button className='delete-letter' onClick={()=> this.props.delEmail(item.id, this.props.mails)}>del</button>

                    </li>

                </div>)
        })


        return (
            <div className="mail-list">
                <ul>
                    {mailList}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        mails: state.mails.mailList,
        isFetching: state.mails.isFetching
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        delEmail: (delID, mailList) => dispatch(delEmail(delID, mailList))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(MailList)